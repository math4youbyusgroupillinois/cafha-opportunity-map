# CAFHA Opportunity Map

## Running locally

``` bash
git clone git@github.com:datamade/cafha-opportunity-map.git
cd cafha-opportunity-map

# to run locally
python -m SimpleHTTPServer
```

navigate to http://localhost:8000/

# Data


# Dependencies
We used the following open source tools:

* [Bootstrap](http://getbootstrap.com/) - Responsive HTML, CSS and Javascript framework
* [Leaflet](http://leafletjs.com/) - javascript library interactive maps
* [jQuery Address](https://github.com/asual/jquery-address) - javascript library creating RESTful URLs
* [GitHub pages](https://pages.github.com/) - free static website hosting

## Errors / Bugs

If something is not behaving intuitively, it is a bug, and should be reported.
Report it here: https://github.com/datamade/cafha-opportunity-map/issues

## Note on Patches/Pull Requests
 
* Fork the project.
* Make your feature addition or bug fix.
* Commit, do not mess with rakefile, version, or history.
* Send me a pull request. Bonus points for topic branches.

## Copyright

Copyright (c) 2014 DataMade and Chicago Area Fair Housing Alliance. Released under the [MIT License](https://github.com/datamade/cafha-opportunity-map/blob/master/LICENSE).